# Various Topics

## App Life Cycle

There is a main() function as in any C based App. This hands off to the UI Framework through UIApplicationMain().  

https://developer.apple.com/library/content/documentation/iPhone/Conceptual/iPhoneOSProgrammingGuide/TheAppLifeCycle/TheAppLifeCycle.html

An app’s main run loop processes all user-related events. The UIApplication object sets up the main run loop at launch time and uses it to process events and handle updates to view-based interfaces. As the name suggests, the main run loop executes on the app’s main thread. This behavior ensures that user-related events are processed serially in the order in which they were received.

Most state transitions are accompanied by a corresponding call to the methods of your app delegate object. These methods are your chance to respond to state changes in an appropriate way. These methods are listed below, along with a summary of how you might use them.

application:willFinishLaunchingWithOptions:—This method is your app’s first chance to execute code at launch time.

application:didFinishLaunchingWithOptions:—This method allows you to perform any final initialization before your app is displayed to the user.

applicationDidBecomeActive:—Lets your app know that it is about to become the foreground app. Use this method for any last minute preparation.

applicationWillResignActive:—Lets you know that your app is transitioning away from being the foreground app. Use this method to put your app into a quiescent state.

applicationDidEnterBackground:—Lets you know that your app is now running in the background and may be suspended at any time.

applicationWillEnterForeground:—Lets you know that your app is moving out of the background and back into the foreground, but that it is not yet active.

applicationWillTerminate:—Lets you know that your app is being terminated. This method is not called if your app is suspended.


## Bitcode

http://lowlevelbits.org/bitcode-demystified/

## Autolayout performance

http://floriankugler.com/2013/04/22/auto-layout-performance-on-ios/

## Property Modifiers

atomic vs nonatomic primarily ensures that complete values are returned from synthesized getters and that complete values are written by synthesized setters (atomic is default.)

readwrite vs readonly determines whether a synthesized property has a synthesized accessor or not (readwrite has a setter and is the default, readonly does not).

assign vs retain vs copy determines how the synthesized accessors interact with the Objective-C memory management scheme:

assign is the default and simply performs a variable assignment
retain specifies the new value should be sent -retain on assignment and the old value sent -release
copy specifies the new value should be sent -copy on assignment and the old value sent -release

## NS_ENUM and NS_OPTIONS

http://nshipster.com/ns_enum-ns_options/

The first argument for NS_ENUM is the type used to store the new type. In a 64-bit environment, Make sure that the specified size can fit all of the defined values, or else an error will be generated. The second argument is the name of the new type (as you probably guessed). Inside the block, the values are defined as usual.
