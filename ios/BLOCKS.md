__block is a storage type that is use to make in scope variables mutable, more frankly if you declare a variable with this specifier, its reference will pe passed to blocks not a read-only copy for more details see Blocks Programming in iOS


https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/WorkingwithBlocks/WorkingwithBlocks.html


OBJC

http://kmithi.blogspot.in/2012/03/blocks-programming-in-ios.html

^(parameter list){
   //you code
}

__block BOOL result = NO;
dispatch_sync(dispatch_get_main_queue(), ^{
  ...
  result = YES;
  ...
});
You should use __weak if you want to avoid retain cycles.
e.g.:
__weak typeof(self) wself = self;
self.foobarCompletion = ^{
  ...
  wself.foo = YES;
  ...
};
