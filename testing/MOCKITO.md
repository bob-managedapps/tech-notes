https://github.com/mockito/mockito/tree/master

https://code.google.com/archive/p/mockito/

http://site.mockito.org/

Main reference documentation features:

mock()/@Mock: create mock
optionally specify how it should behave via Answer/ReturnValues/MockSettings
when()/given() to specify how a mock should behave
If the provided answers don’t fit your needs, write one yourself extending the Answer interface
spy()/@Spy: partial mocking, real methods are invoked but still can be verified and stubbed
@InjectMocks: automatically inject mocks/spies fields annotated with @Spy or @Mock
verify(): to check methods were called with given arguments
can use flexible argument matching, for example any expression via the any()
or capture what arguments where called using @Captor instead
Try Behavior-Driven development syntax with BDDMockito
Use Mockito on Android, thanks to the Google guys working on dexmaker
