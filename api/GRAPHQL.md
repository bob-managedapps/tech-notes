GraphQL is a query language for your API, and a server side runtime for executing queries. A single endpoint can return data about multiple resources.

https://medium.com/@lachlanmiller_52885/graphql-basics-and-practical-examples-with-vue-6b649b9685e0

```
type Query { // define the query
  me: User // define the fields
}
type User { // define the type
  id: ID
  name: String
}
function Query_me(request) { // define the function
  return request.auth.user
}
```

The above is how we implement a query, custom type, and endpoint using GraphQL. The matching client side query looks like this:


```
{
  me {
   name
  }
}
```

which returns:

```
{
  “me”: {
    “name”: “username”
  }
}
```
Unlike REST APIs, Graph APIs have just one endpoint, which responds to all requests.