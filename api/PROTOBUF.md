# Widely adpoted across google
## Protocol Buffers

xml or json way...
```
<person>
  <name>John Doe</name>
  <email>jdoe@example.com</email>
</person>
```
protobuf way...

define a .proto file

```
message Person {
	required string name = 1;
	required string email = 2;
}
```

Then, you simply run the protobuf compiler on the .proto file to generate a client library that you can directly import into your application.

protoc --python_out=DST_DIR person.proto

```
import person_pb2
 
person = person_pb2.Person()
person.name = "Steven Schmatz"
person.email = "steven@gmail.com"  # I wish I had this email
 
print(person.SerializeToString())
```

Check out the highly optimized XML

```
\n\x0eSteven Schmatz\x12\x10steven@gmail.com
```

Parsing is easy too!

```
import person_pb2
import requests
 
response = requests.get("http://server.com/example").text
person = person_pb2.Person().ParseFromString(response)
 
name = person.name()
email = person.email()
 
print(f"{name} uses the address {email}.")
```