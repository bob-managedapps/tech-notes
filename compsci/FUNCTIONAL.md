In computer science, functional programming is a programming
paradigm—a style of building the structure and elements of computer
programs—that treats computation as the evaluation of mathematical
functions and avoids changing-state and mutable data. It is a
declarative programming paradigm, which means programming is done with
expressions or declarations instead of statements.

Roots are in https://en.wikipedia.org/wiki/Lambda_calculus

Lambda calculus provides a theoretical framework for describing
functions and their evaluation. Although it is a mathematical
abstraction rather than a programming language, it forms the basis of
almost all functional programming languages today. An equivalent
theoretical formulation, combinatory logic, is commonly perceived as
more abstract than lambda calculus and preceded it in
invention. Combinatory logic and lambda calculus were both originally
developed to achieve a clearer approach to the foundations of
mathematics.

Example Erlang

-module(fib).    % This is the file 'fib.erl', the module and the filename must match
-export([fib/1]). % This exports the function 'fib' of arity 1

fib(1) -> 1; % If 1, then return 1, otherwise (note the semicolon ; meaning 'else')
fib(2) -> 1; % If 2, then return 1, otherwise
fib(N) -> fib(N - 2) + fib(N - 1).

https://maryrosecook.com/blog/post/a-practical-introduction-to-functional-programming

------

from random import random

time = 5
car_positions = [1, 1, 1]

while time:
    # decrease time
    time -= 1

    print ''
    for i in range(len(car_positions)):
        # move car
        if random() > 0.3:
            car_positions[i] += 1

        # draw car
        print '-' * car_positions[i]

------

from random import random

def move_cars():
    for i, _ in enumerate(car_positions):
        if random() > 0.3:
            car_positions[i] += 1

def draw_car(car_position):
    print '-' * car_position

def run_step_of_race():
    global time
    time -= 1
    move_cars()

def draw():
    print ''
    for car_position in car_positions:
        draw_car(car_position)

time = 5
car_positions = [1, 1, 1]

while time:
    run_step_of_race()
    draw()


--------functional

from random import random

def move_cars(car_positions):
    return map(lambda x: x + 1 if random() > 0.3 else x,
               car_positions)

def output_car(car_position):
    return '-' * car_position

def run_step_of_race(state):
    return {'time': state['time'] - 1,
            'car_positions': move_cars(state['car_positions'])}

def draw(state):
    print ''
    print '\n'.join(map(output_car, state['car_positions']))

def race(state):
    draw(state)
    if state['time']:
        race(run_step_of_race(state))

race({'time': 5,
      'car_positions': [1, 1, 1]})
