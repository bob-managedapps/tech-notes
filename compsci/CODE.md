## Editors

http://www.marksimonson.com/fonts/view/anonymous-pro

Open emacs in X11, goto menu Options, select "set default font ...", change the font size. Select "save options" in the same menu. Done. It all depends what you mean by change the font size.

http://wesbos.com/programming-fonts/

## Code Quality Static Checking Links

http://fbinfer.com/

## Swift code formatter

https://github.com/nicklockwood/SwiftFormat
