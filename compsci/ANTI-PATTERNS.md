## Telescoping Constructor

The Telescoping Constructor is an example of a pattern that borders on an anti-pattern that is all too often used in projects even though there are better alternatives availble. In this pattern, your POJO has numerous constructors each taking a different number of parameters that, if the class has been written correctly, delegate to a default constructor.telescoping constructor

http://www.captaindebug.com/2011/05/telescoping-constructor-antipattern.html#.WCUtfqJ95Go

```public FoodTelescopingDemo(int id, String name) {
       this(id, name, 0, 0, 0, "default description");
}

public FoodTelescopingDemo(int id, String name, int calories) {
       this(id, name, calories, 0, 0, "default description");
}

public FoodTelescopingDemo(int id, String name, int calories, int servingSize) {
       this(id, name, calories, servingSize, 0, "default description");
}

public FoodTelescopingDemo(int id, String name, int calories, int servingSize, int fat) {
       this(id, name, calories, servingSize, fat, "default description");
}```

## Are Managers a sign of bad software engineering?

Having lots of "manager" classes is often a sympton of an anemic domain model, where the domain logic is hoisted out of the domain model and instead placed in manager classes, which more or less equate to transaction scripts. The danger here is that you're basically reverting to procedural programming - that in itself may or may not be a good thing depending on your project - but the fact that it wasn't considered or intended is the real problem imo.

Following the principle of the "information expert", a logical operation should reside as close to the data it requires as possible. This would mean moving domain logic back into the domain model, so that it's these logical operations which have an observable effect on the state of the domain model, rather than 'managers' changing the state of the domain model from the outside.

When the class has "Manager" in the name, beware of the god class problem (one with too many responsibilities). If it is difficult to describe what a class is responsible for with its name, that is a design warning sign.

The name FooManager says nothing about what the class actually does, except that it somehow involves Foo instances. Giving the class a more meaningful name elucidates its true purpose, which will likely lead to refactoring.
