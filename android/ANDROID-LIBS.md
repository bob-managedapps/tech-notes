# Glide

Glide is a fast and efficient open source media management and image loading framework for Android that wraps media decoding, memory and disk caching, and resource pooling into a simple and easy to use interface.

https://github.com/bumptech/glide

# Realm

Data sychronization to server.

https://realm.io/news/realm-for-android/

# LeakCanary

Lead detection for android

https://github.com/square/leakcanary

# AssertJ

Fluent assertions

http://joel-costigliola.github.io/assertj/

# Picasso

Image downloading and caching

http://square.github.io/picasso/

# Fresco

Image Management Libary

http://frescolib.org/

# RxAndroid

Reactive Extensions for Android using RxJava

https://github.com/ReactiveX/RxAndroid

# OkHTTP

Square HTTP Library

http://square.github.io/okhttp/

# Retrofit

HTTP Library

https://square.github.io/retrofit/

# Butterknife

Field and Method binding

http://jakewharton.github.io/butterknife/

# Dagger

Dependency Injection

https://guides.codepath.com/android/Dependency-Injection-with-Dagger-2

# Mosby

Model-View-Presenter library

http://hannesdorfmann.com/mosby/

https://github.com/sockeqwe/mosby

# Volley

Networking Library

https://developer.android.com/training/volley/index.html

# RoboElectric

[TDD Unit Test Framework runs inside JVM](http://robolectric.org/)




