POJO

POJO means Plain Old Java Object. It refers to a Java object (instance of definition) that isn't bogged down by framework extensions. For example, to receive messages from JMS, you need to write a class that implements the MessageListener interface.

Java 8 New Types

https://blogs.oracle.com/java-platform-group/entry/java_8_s_new_type

@NonNull – The compiler can determine cases where a code path might receive a null value, without ever having to debug a NullPointerException.
@ReadOnly – The compiler will flag any attempt to change the object.  This is similar to Collections.unmodifiableList, but more general and verified at compile time.
@Regex – Provides compile-time verification that a String intended to be used as a regular expression is a properly formatted regular expression.
@Tainted and @Untainted – Identity types of data that should not be used together, such as remote user input being used in system commands, or sensitive information in log streams.
@m – Units of measure ensures that numbers used for measuring objects are used and compared correctly, or have undergone the proper unit conversion.