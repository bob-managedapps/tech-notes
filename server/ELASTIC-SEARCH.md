Elasticsearch is a search engine based on Lucene. It provides a
distributed, multitenant-capable full-text search engine with an HTTP
web interface and schema-free JSON documents. Elasticsearch is
developed in Java and is released as open source under the terms of the Apache License. Elasticsearch is the most popular enterprise search engine followed by Apache Solr, also based on Lucene.


http://joelabrahamsson.com/elasticsearch-101/
