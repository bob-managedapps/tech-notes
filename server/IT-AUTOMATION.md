http://www.actoncloud.com/blog/configuration-management-tools-ansible-puppet-chef/

Ansible

Puppet

Chef

Salt stack

https://www.ansible.com/

Ansible is a free-software platform for configuring and managing computers which combines multi-node software deployment, ad hoc task execution, and configuration management.[1] It manages nodes over SSH or over PowerShell.[2] Modules work over JSON and standard output and can be written in any programming language. The system uses YAML to express reusable descriptions of systems.[3]

Chef understands and based on pure Ruby on Rails whereas Puppet is a ruby like wrapper written on top of Ruby on Rails. Both are based on agents and they are developer oriented tools. Developer need to write many lines of code and manage. But when we have to scale, we can’t be depending on so many ssh connections and hence Ansible scores less here and people need to choose between Chef and Ruby.

