# Fetch vs Pull

https://longair.net/blog/2009/04/16/git-fetch-and-merge/

# Merge vs Rebase

Rebases are how changes should pass from the top of hierarchy downwards and merges are how they flow back upwards.
https://www.derekgourlay.com/blog/git-when-to-merge-vs-when-to-rebase/
To avoid messy merge commits and help keep a relatively clean git commit history use the following workflow when fetching upstream changes:

git fetch origin
git rebase −p origin/develop


# Good Branch Naming

marketing/feature/AM-258/update-to-latest-appboy-sdk

name/feature/issue-tracker-number/short-description

# Command Line Graph

git log --graph --oneline --decorate --branches="feature/*"

# For an Advanced command line experience with GIT on OSX

http://neverstopbuilding.com/gitpro

# For Emacs

https://magit.vc/

Magit is an interface to the version control system Git, implemented as an Emacs package. Magit aspires to be a complete Git porcelain. While we cannot (yet) claim that Magit wraps and improves upon each and every Git command, it is complete enough to allow even experienced Git users to perform almost all of their daily version control tasks directly from within Emacs. While many fine Git clients exist, only Magit and Git itself deserve to be called porcelains.