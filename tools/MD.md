# The largest heading
## The second largest heading
###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

> Quote something here

Use some `code snippet` here

Some basic git commands
```
git status
git add
dit commit
```

This site was built using [GitHub Pages](https://pages.github.com/).

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

1. Make my changes
  1. Fix bug
  2. Improve formatting
    * Make the headings bigger
2. Push my commits to GitHub
3. Open a pull request
  * Describe my changes
  * Mention all the members of my team
    * Ask for feedback

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request

@github/support What do you think about these updates?

@octocat :+1: This PR looks great - it's ready to merge! :shipit:

Let's rename \*our-new-project\* to \*our-old-project\*.

