## Using Shell in Emacs

https://www.masteringemacs.org/article/running-shells-in-emacs-overview

## OSX Specific things

http://batsov.com/articles/2012/10/14/emacs-on-osx/

## Lisp

http://letoverlambda.com/index.cl/guest/chap5.html
https://www.gnu.org/software/emacs/manual/html_node/emacs/Lisp-Eval.html

## Programming modes

https://github.com/swift-emacs/swift-mode
http://jblevins.org/projects/markdown-mode/

