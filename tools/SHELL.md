## How to see environment variables on a mac
```
echo $PATH
echo $HOME
```

## Mac Terminal

https://www.iterm2.com/

## How to add to your path on a mac to the current shell
```
vi $HOME/.bash_profile
source $HOME/.bash_profile
echo $PATH
```

Format to set the path enviornment variable in your .bash_profile file
```
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
```

## What source does

source is a Unix command that evaluates the file following the command, as a list of commands, executed in the current context. Frequently the "current context" is a terminal window into which the user is typing commands during an interactive session.

## What the Bash shell loads

bash as login shell will load /etc/profile, ~/.bash_profile, ~/.bash_login, ~/.profile in the order
